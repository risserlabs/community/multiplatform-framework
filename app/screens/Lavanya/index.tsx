/**
 * File: /screens/Lavanya/index.tsx
 * Project: app
 * File Created: 29-09-2022 23:11:25
 * Author: Lavanya
 * -----
 * Last Modified: 01-10-2022 05:42:20
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { Box } from 'app/components/elements/Box';
import { Text } from 'app/components/elements/Text';
import { createParam } from '@risserlabs/solito/build';

const { useParam } = createParam();

export const LavanyaScreen = () => {
  const [lavanyaColor] = useParam('lavanyaColor');
  console.log('color', lavanyaColor);
  return (
    <Box
      style={{
        alignItems: 'center',
        backgroundColor: lavanyaColor,
      }}
    >
      <Text style={{ backgroundColor: lavanyaColor }}>Katari Lavanya favorite color {lavanyaColor}</Text>
    </Box>
  );
};
