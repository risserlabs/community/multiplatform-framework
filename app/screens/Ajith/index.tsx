/**
 * File: /screens/Ajith/index.tsx
 * Project: @multiplatform/next
 * File Created: 28-09-2022 06:46:12
 * Author: Ajith Kumar
 * -----
 * Last Modified: 01-10-2022 04:26:14
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { Header, Box } from 'app/components';
import { createParam } from '@risserlabs/solito/build';

const { useParam } = createParam();

export const AjithScreen = () => {
  const [ajithColor] = useParam('ajithColor');
  console.log('ajithColor', ajithColor);
  return (
    <Box style={{ backgroundColor: ajithColor }}>
      <Header description="regarding Fav color">My name is Ajith and my Fav color is {ajithColor}</Header>
    </Box>
  );
};
