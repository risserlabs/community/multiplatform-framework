/**
 * File: /screens/Formik/index.tsx
 * Project: app
 * File Created: 26-10-2022 10:05:04
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 17:03:20
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Box, Input, Button, Text } from 'app/components';
import { Formik } from 'formik';
import { Spinner } from '@native-theme-ui/core';
import * as Yup from 'yup';

const FormSchema = Yup.object().shape({
  email: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Required'),
});

export const FormikScreen = () => {
  return (
    <Box>
      <Formik
        initialValues={{ firstName: '', email: '' }}
        // validate={(values) => {
        //   const errors: Record<string, unknown> = {};
        //   const isEmail = !!values.email.match(/^.+@.+\..+$/g);
        //   if (!isEmail) errors.email = 'invalid email';
        //   return errors;
        // }}
        validationSchema={FormSchema}
        onSubmit={async () => {
          await new Promise((r: any) => setTimeout(r, 3000));
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors, isSubmitting }) => (
          <>
            <Box>{isSubmitting?.toString()}</Box>
            <Text sx={{ color: 'red' }}>{errors.firstName}</Text>
            <Input onChangeText={handleChange('firstName')} onBlur={handleBlur('firstName')} value={values.firstName} />
            <Text sx={{ color: 'red' }}>{errors.email}</Text>
            <Input onChangeText={handleChange('email')} onBlur={handleBlur('email')} value={values.email} />
            <Button onPress={() => handleSubmit()}>Submit</Button>
            <Box>{JSON.stringify(errors)}</Box>
            {isSubmitting && <Spinner />}
          </>
        )}
      </Formik>
    </Box>
  );
};
