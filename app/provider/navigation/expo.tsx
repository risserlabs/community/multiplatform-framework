/**
 * File: /provider/navigation/expo.tsx
 * Project: app
 * File Created: 19-08-2022 07:07:26
 * Author: Clay Risser
 * -----
 * Last Modified: 31-10-2022 11:00:34
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import * as Linking from 'expo-linking';
import { useMemo } from 'react';
import { ProviderProps } from '..';
import { routeMaps } from '../../navigation/native';

const initialRouteName = Object.entries(routeMaps).reduce(
  (initialRouteName: string, [key, value]: [string, string]) => {
    if (initialRouteName !== '') return initialRouteName;
    if (value === '') initialRouteName = key;
    return initialRouteName;
  },
  '',
);

const domain = 'multiplatform.one';

export const NavigationProvider: FC<ProviderProps> = ({ children }: ProviderProps) => {
  return (
    <NavigationContainer
      linking={
        useMemo(
          () => ({
            prefixes: [
              Linking.createURL('/'),
              `https://${domain}/`,
              `https://*.${domain}/`,
              `http://${domain}/`,
              `http://*.${domain}/`,
            ],
            config: {
              initialRouteName,
              screens: routeMaps,
            },
          }),
          [],
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ) as any
      }
    >
      {children}
    </NavigationContainer>
  );
};
