/**
 * File: /provider/theme.tsx
 * Project: -
 * File Created: 18-08-2022 06:38:22
 * Author: Clay Risser
 * -----
 * Last Modified: 25-11-2022 10:49:12
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { DripsyProvider } from 'dripsy';
import { MultiPlatform } from 'multiplatform.one';
import { ProviderProps } from './index';

export interface ThemeProviderProps extends ProviderProps {
  theme: any;
}

export const ThemeProvider: FC<ThemeProviderProps> = (props: ThemeProviderProps) => (
  <DripsyProvider theme={props.theme} ssr={MultiPlatform.isNext()}>
    {props.children}
  </DripsyProvider>
);
