/**
 * File: /organisms/TodoListsBody/index.tsx
 * Project: app
 * File Created: 26-09-2022 00:50:59
 * Author: Ajith Kumar
 * -----

 * Last Modified: 02-12-2022 14:09:22
 * Modified By: K S R P BHUSHAN

 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { useState } from 'react';
import { Input, Button, Box, Flex, Text } from '@native-theme-ui/core';
import { useRecoilState } from 'recoil';
import { taskAtom } from '../../atoms';
import { TodoListsItems } from '../../components/molecules/TodoListsItems';


export const TodoListsBody = () => {
  const [input, setInput] = useState('');
  const [list, setList] = useRecoilState(taskAtom);

  const onSubmitHandler = () => {
    console.log(input);
    if (input === '') {
      return;
    } else {
      const newTodo: string[] = [...list, input];
      setList(newTodo);
      setInput('');
    }
  };

  const onDeleteHandler = (indexValue: number) => {
    const newList = list.filter((_todo: string, index: number) => index !== indexValue);
    setList(newList);
  };

  return (
    <Box
      style={{ alignItems: 'center' }}
      fromSx={{ scale: 0 }}
      animateSx={{ scale: 1 }}
      transition={{ type: 'timing', duration: 500 }}
    >
      <Flex>
        <Input
          style={{ backgroundColor: '#FFD700' }}
          placeholder="Enter task to create"
          placeholderTextColor='#36454F'
          value={input}
          onChangeText={(value) => setInput(value)}
        />
        <Text>&nbsp;&nbsp;</Text>
        <Box>
          <Button style={{ backgroundColor: '#FFD700' }} onPress={onSubmitHandler}>
            Add
          </Button>
        </Box>
      </Flex>
      <TodoListsItems data={list} deleteList={onDeleteHandler} />
    </Box>
  );
};
