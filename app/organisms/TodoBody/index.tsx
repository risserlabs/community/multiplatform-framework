/**
 * File: /organisms/TodoBody/index.tsx
 * Project: app
 * File Created: 13-09-2022 23:07:59
 * Author: Ajith Kumar
 * -----
 * Last Modified: 02-12-2022 14:07:10
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useState } from 'react';
import { Text } from '@native-theme-ui/core';
import { todoAtom } from '../../atoms';
import { useRecoilState } from 'recoil';
import { TodoItems } from '../../components/molecules/TodoItems';
import { Box } from '../../components/elements/Box';
import { Button } from '../../components/elements/Button';
import { Flex } from '../../components/elements/Flex';
import { Input } from '../../components/elements/Input';

export const TodoBody: FC<unknown> = () => {
  const [task, setTask] = useState('');
  const [toDos, setToDos] = useRecoilState(todoAtom);

  const handleChangeText = (text: string) => {
    setTask(text);
  };

  const onSubmitHandler = () => {
    console.log(task);
    if (task === '') {
      return;
    } else {
      const newTodo: string[] = [...toDos, task];
      setToDos(newTodo);
    }
    console.log(toDos);
    setTask('');
  };

  const onDeleteHandler = (indexValue: number) => {
    const deletedTodos = toDos.filter((_todo: string, index: number) => index !== indexValue);
    setToDos(deletedTodos);
  };

  return (
    <Box
      style={{
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'yellow',
        padding: 10,
      }}
    >
      <Flex>
        <Input
          style={{ backgroundColor: 'black' }}
          placeholder="Enter Task"
          value={task}
          onChangeText={handleChangeText}
        />
        <Text>&nbsp;&nbsp;</Text>
        <Button style={{ backgroundColor: 'black' }} onPress={onSubmitHandler}>
          Add
        </Button>
      </Flex>
      <TodoItems todos={toDos} onDeletedTodo={onDeleteHandler} />
    </Box>
  );
};
