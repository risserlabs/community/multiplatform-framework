/**
 * File: /components/molecules/TodoItems/index.tsx
 * Project: app
 * File Created: 14-09-2022 01:14:15
 * Author: Ajith Kumar
 * -----
 * Last Modified: 01-12-2022 09:45:03
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Box, Flex } from '@native-theme-ui/core';
import { TodoItem } from '../TodoItem';

export interface TodoItemsProps {
  todos: string[];
  onDeletedTodo: (index: number) => void;
}

export const TodoItems: FC<TodoItemsProps> = ({ todos, onDeletedTodo }: TodoItemsProps) => {
  return (
    <Box
      style={{ alignItems: 'center' }}
      fromSx={{ scale: 0 }}
      animateSx={{ scale: 1 }}
      transition={{ type: 'timing', duration: 300 }}
    >
      {todos.map((todo: string, index: number) => (
        <Box
          key={index}
          style={{
            alignItems: 'center',
            alignContent: 'center',
          }}
        >
          <Flex alignItems="center">
            <TodoItem onDeletedTodo={() => onDeletedTodo(index)}>{todo}</TodoItem>
          </Flex>
        </Box>
      ))}
    </Box>
  );
};
