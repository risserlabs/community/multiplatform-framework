/**
 * File: /components/molecules/TodoListsItems/index.tsx
 * Project: app
 * File Created: 26-09-2022 23:23:11
 * Author: Ajith Kumar
 * -----
 * Last Modified: 07-10-2022 00:23:21
 * Modified By: Lavanya
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Box } from 'app/components/elements';
import { TodoListsItem } from '../TodoListsItem';
import { Link } from '@risserlabs/solito/link';

export interface TodoListsItemsProps {
  data: string[];
  deleteList: (index: number) => void;
}

export const TodoListsItems: FC<TodoListsItemsProps> = ({ data, deleteList }: TodoListsItemsProps) => {
  return (
    <Box>
      {data.map((menuList: string, index: number) => (
        <Box key={index} style={{ alignContent: 'center', alignItems: 'center' }}>
          <Link href="/todo-lists/todo-list">
            <TodoListsItem deleteList={() => deleteList(index)}>{menuList}</TodoListsItem>
          </Link>
        </Box>
      ))}
    </Box>
  );
};
