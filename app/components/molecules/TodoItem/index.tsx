/**
 * File: /components/molecules/TodoItem/index.tsx
 * Project: app
 * File Created: 23-09-2022 23:22:15
 * Author: Ajith Kumar
 * -----
 * Last Modified: 02-12-2022 17:59:36
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';

import { Box, Checkbox, Flex, Button, Text, H3 } from '@native-theme-ui/core';

export interface TodoItemProps {
  children?: string;
  onDeletedTodo: () => void;
}

export const TodoItem: FC<TodoItemProps> = (props: TodoItemProps) => {
  return (
    <Box fromSx={{ scale: 0 }} animateSx={{ scale: 1 }} transition={{ type: 'timing', duration: 800 }}>
      <Flex
        style={{
          alignItems: 'center',
          flexWrap: 'wrap',
          alignContent: 'space-around',
          // marginTop: 10,
          padding: 10,
          borderRadius: 20,
          // borderRadius: 20,
        }}
      >
        <Checkbox style={{ borderColor: '#FFD700' }} />
        <Text>&nbsp;&nbsp;</Text>
        <H3>{props.children}</H3>
        <Text>&nbsp;&nbsp;</Text>
        <Box>
          <Button
            style={{
              backgroundColor: 'lightBlue',
            }}
            onPress={() => props.onDeletedTodo()}
          >
            Delete
          </Button>
        </Box>
      </Flex>
    </Box>
  );
};
