/**
 * File: /navigation/native.tsx
 * Project: app
 * File Created: 23-09-2022 05:24:12
 * Author: Clay Risser
 * -----

 * Last Modified: 02-12-2022 17:15:42
 * Modified By: K S R P BHUSHAN

 * Last Modified: 02-12-2022 17:15:42
 * Modified By: K S R P BHUSHAN

 * Last Modified: 02-12-2022 17:15:42
 * Modified By: K S R P BHUSHAN

 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from '../screens/Home';
import { TodoListScreen } from '../screens/TodoList';

import { ManikantaScreen } from '../screens/Manikanta';

import { AjithScreen } from '../screens/Ajith';

import { LavanyaScreen } from '../screens/Lavanya';
import { TodoListsScreen } from '../screens/TodoLists';

const Stack = createNativeStackNavigator();

export const NativeNavigation = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <Stack.Screen
      name="todoLists"
      component={TodoListsScreen}
      options={{
        title: 'TodoLists',
      }}
    />
    <Stack.Screen
      name="home"
      component={HomeScreen}
      options={{
        title: 'Home',
      }}
    />
    <Stack.Screen
      name="lavanya"
      component={LavanyaScreen}
      options={{
        title: 'Lavanya',
      }}
    />
    <Stack.Screen
      name="lavanyaColor"
      component={LavanyaScreen}
      options={{
        title: 'LavanyaColor',
      }}
    />

    <Stack.Screen
      name="manikanta"
      component={ManikantaScreen}
      options={{
        title: 'Manikanta',
      }}
    />
    <Stack.Screen
      name="manikantaColor"
      component={ManikantaScreen}
      options={{
        title: 'ManikantaColor',
      }}
    />

    <Stack.Screen
      name="todoList"
      component={TodoListScreen}
      options={{
        title: 'TodoList',
      }}
    />

    <Stack.Screen
      name="ajith"
      component={AjithScreen}
      options={{
        title: 'Ajith',
      }}
    />
    <Stack.Screen
      name="ajithFavColor"
      component={AjithScreen}
      options={{
        title: 'AjithColor',
      }}
    />

    {/* <Stack.Screen
      name="clay"
      component={ClayScreen}
      options={{
        title: "TodoLists",
      }}
    /> */}
    {/* <Stack.Screen
      name="clayColor"
      component={ClayScreen}
      options={{
        title: "TodoLists",
      }}
    />  */}
  </Stack.Navigator>
);

export const routeMaps = {
  lavanya: 'lavanya',
  lavanyaColor: 'lavanya/:color',
  todoLists: 'todo-lists',
  manikanta: 'manikanta',
  manikantaColor: 'manikanta/:manikantaColor',
  home: 'home',
  clay: 'clay',
  clayColor: 'clay/:color',
  todoList: 'todo-lists/:todoListName',
  ajith: 'ajith',
  ajithFavColor: 'ajith/:ajithColor',
};
