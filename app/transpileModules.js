/**
 * File: /transpileModules.js
 * Project: app
 * File Created: 19-08-2022 12:42:03
 * Author: Clay Risser
 * -----
 * Last Modified: 11-11-2022 03:11:01
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const transpileModules = require('@native-theme-ui/core/transpileModules');
const pkg = require('./package.json');

module.exports = [...new Set([...transpileModules, ...pkg.transpileModules])];
