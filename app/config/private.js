/**
 * File: /config/private.js
 * Project: app
 * File Created: 19-11-2022 12:53:12
 * Author: Clay Risser
 * -----
 * Last Modified: 19-11-2022 12:59:14
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module.exports = {
  KEYCLOAK_SSR: process.env.KEYCLOAK_SSR,
  NEXT_AUTOMATIC_STATIC_OPTIMIZATION: process.env.NEXT_AUTOMATIC_STATIC_OPTIMIZATION,
};
