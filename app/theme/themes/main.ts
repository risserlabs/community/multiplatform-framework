/**
 * File: /theme/themes/main.ts
 * Project: app
 * File Created: 26-10-2022 07:47:19
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 16:11:28
 * Modified By: K S R P BHUSHAN
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DripsyBaseTheme, makeTheme } from 'dripsy';
import { Platform } from 'react-native';
import { wrapTheme } from '../util';
import { base } from './base';

export const main = wrapTheme(
  makeTheme({
    ...base,
    autoContrast: true,
    colors: {
      ...base.colors,
      textPrimary: '#ffffff',
      highlight: 'lightBlue',
    },
    types: {
      ...base.types,
      strictVariants: false,
    },
    fonts: {
      ...base.fonts,
      ...Platform.select({
        ios: {
          body: 'Arial',
          h1: 'Arial',
          heading: 'Arial',
          paragraph: 'Arial',
        },
        android: {
          body: 'Roboto',
          h1: 'Roboto',
          heading: 'Roboto',
          paragraph: 'Roboto',
        },
        web: {
          body: 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
          h1: 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
          heading: 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
          paragraph: 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", sans-serif',
        },
      }),
    },
    fontWeights: {
      ...base.fontWeights,
      body: '400',
      heading: '700',
      paragraph: '400',
    },
  } as DripsyBaseTheme),
);
