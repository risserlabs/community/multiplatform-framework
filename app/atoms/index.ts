/**
 * File: /atoms/index.ts
 * Project: app
 * File Created: 13-09-2022 23:21:19
 * Author: Ajith Kumar
 * -----
 * Last Modified: 27-11-2022 03:53:18
 * Modified By: Clay Risser

 * Last Modified: 27-11-2022 03:53:18
 * Modified By: Clay Risser

 * Last Modified: 27-11-2022 03:53:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { atom } from 'recoil';

export const taskAtom = atom<string[]>({
  key: 'taskAtom',
  default: [],
});

export const todoAtom = atom<string[]>({
  key: 'todoAtom',
  default: [],
});
