/**
 * File: /@types/extensions.d.ts
 * Project: app
 * File Created: 13-10-2022 05:06:20
 * Author: Clay Risser
 * -----
 * Last Modified: 31-10-2022 11:00:34
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare module '*.png' {
  const str: string;
  export = str;
}

declare module '*.svg' {
  const str: string;
  export = str;
}
