/**
 * File: /webpack.config.js
 * Project: @multiplatform/expo
 * File Created: 23-01-2022 02:18:40
 * Author: Clay Risser
 * -----
 * Last Modified: 11-11-2022 05:34:50
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-var-requires */

const createExpoWebpackConfigAsync = require('@expo/webpack-config');
const transpileModules = require('app/transpileModules');
const pkg = require('./package.json');

module.exports = async (config, argv) => {
  const webpackConfig = await createExpoWebpackConfigAsync(
    {
      ...config,
      babel: {
        dangerouslyAddModulePathsToTranspile: [...new Set([...transpileModules, ...pkg.transpileModules])],
      },
    },
    argv,
  );
  webpackConfig.resolve.extensions = [
    '.expo-web.js',
    '.expo-web.jsx',
    '.expo-web.mjs',
    '.expo-web.ts',
    '.expo-web.tsx',
    ...webpackConfig.resolve.extensions,
  ];
  return webpackConfig;
};
